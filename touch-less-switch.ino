/*
* Touchless Switch
*
*/

// defines pins numbers
const int trigPin = D1;
const int echoPin = D2;

const int relay = D0;
boolean onoff=0;

int count = 0;

// defines variables
long duration;
int distance;

void setup() {
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input

pinMode(relay, OUTPUT); // Sets the relay as an Output
digitalWrite(relay, onoff);

Serial.begin(9600); // Starts the serial communication
}

void loop() {
// Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);

// Calculating the distance
distance= duration*0.034/2;

// Prints the distance on the Serial Monitor
//Serial.print("Distance: ");
//Serial.println(distance);
//delay(2000);

if(distance<10)   count++;

Serial.println(count);

if(count>2)  { onoff=!onoff; digitalWrite(relay, onoff);  count=0; delay(1500); }

delay(500);
}